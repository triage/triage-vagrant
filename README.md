Configuración Vagrant para Triage
=================================

Esta configuración Vagrant genera un ambiente de test/producción (no de desarrollo) para la aplicación Triage.

Está basada en Ubuntu/Trusty 64 y utiliza Puppet para realizar el provisioning.

Instrucciones de uso:

1. Clonar este repositorio
2. Ejecutar Vagrant (_vagrant up_)
3. Navegar http://localhost:8080
4. Usuario: luis, password: triage
